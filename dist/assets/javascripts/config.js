System.register(["./routes"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var routes_1;
    function config($stateProvider) {
        routes_1.default.routes.map(function (route) {
            if (route.abstract) {
                if (route.templateUrl == undefined && route.template == undefined) {
                    route.template = "<div ui-view></div>";
                }
            }
            else {
                route.templateUrl = route.templateUrl || "/views/" + route.name + ".html";
            }
            console.log(route);
            $stateProvider.state(route);
        });
    }
    return {
        setters:[
            function (routes_1_1) {
                routes_1 = routes_1_1;
            }],
        execute: function() {
            config.$inject = ["$stateProvider"];
            exports_1("default",config);
        }
    }
});
//# sourceMappingURL=config.js.map