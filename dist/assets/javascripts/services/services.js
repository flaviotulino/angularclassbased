var services;
(function (services) {
    var AuthService = (function () {
        function AuthService() {
            this.token = null;
        }
        AuthService.prototype.setToken = function (token) {
            this.token = token;
            this.header = {
                authorization: token
            };
        };
        AuthService.prototype.storeToken = function (permanently) {
            if (this.token == undefined || this.token == null) {
                console.error('Token has not been set yet');
                return false;
            }
            else if (permanently) {
                localStorage.setItem("accessToken", this.token);
            }
            else {
                sessionStorage.setItem("accessToken", this.token);
            }
        };
        AuthService.prototype.getToken = function () {
            return localStorage.getItem('accessToken') || sessionStorage.getItem('accessToken') || this.token;
        };
        AuthService.prototype.getAuthorizationHeader = function () {
            return this.header;
        };
        return AuthService;
    }());
    services.AuthService = AuthService;
})(services || (services = {}));
