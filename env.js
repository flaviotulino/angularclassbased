var glob = require("glob");
var fs = require("fs");
var util = require('util');

var config = JSON.parse(fs.readFileSync('env.config.json', 'utf-8'));

var args = process.argv.slice(2);
console.log(args);

// Generate the api config file
var api = config.api;
var configFileCode = "\/**\r\n * DOC URL : " + (api._docUrl || 'no url given') + "\r\n */\r\nvar config = {\r\n    DEV_ENDPOINT: \'{{DEV_ENDPOINT_HERE}}\',\r\n    PRODUCTION_ENDPOINT: \'{{PRODUCTION_ENDPOINT_HERE}}\'\r\n};\r\nexport default config;";
//fs.writeFileSync('api/config.ts');
//Object.keys[api].map(function(key) {
configFileCode = configFileCode.replace(/\{\{DEV_ENDPOINT_HERE\}\}/gi, api.DEV_ENDPOINT);
configFileCode = configFileCode.replace(/\{\{PRODUCTION_ENDPOINT_HERE\}\}/gi, api.PRODUCTION_ENDPOINT);
//});

fs.writeFileSync('api/config.ts', configFileCode);


// Merge locals using the config file
function mergeLocales() {
    var output = undefined;
    var locales = config.locales;
    locales.map(function (lang) {
        glob("locales/**/" + lang + ".json", {}, function (er, files) {
            files.map(function (file) {
                var content = fs.readFileSync(file, 'utf8');
                if (output === undefined) {
                    output = JSON.parse(content);
                } else {
                    output = Object.assign(output, JSON.parse(content));
                }

                fs.watch(file, function (event, filename) {
                    if (filename.indexOf('bundle.json') < 0) {
                        mergeLocales();
                    }
                });
            });

            if (output !== undefined) {
                fs.writeFileSync('locales/' + lang + ".bundle.json", JSON.stringify(output));
                //fs.writeFileSync('assets/javascripts/locales/' + lang + ".js", 'var locales=' + util.inspect(output,false,2,false) + ';');
            }
        });
    });
}
mergeLocales();



