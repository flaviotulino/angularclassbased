module services {
    export class AuthService {
        private token = null;
        private header;

        setToken(token) {
            this.token = token;
            this.header = {
                authorization: token
            }
        }

        storeToken(permanently?:boolean) {
            if (this.token == undefined || this.token == null) {
                console.error('Token has not been set yet');
                return false;
            } else if (permanently) {
                localStorage.setItem("accessToken", this.token);
            } else {
                sessionStorage.setItem("accessToken", this.token);
            }
        }

        getToken():string {
            return localStorage.getItem('accessToken') || sessionStorage.getItem('accessToken') || this.token;
        }

        getAuthorizationHeader():{authorization:string} {
            return this.header;
        }
    }
}