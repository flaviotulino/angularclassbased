System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var BaseController;
    return {
        setters:[],
        execute: function() {
            /**
             * This is the base controller class.
             *
             * Auto-injects the angular $scope and creates a reference to it,
             * using the state name by convention
             *
             */
            BaseController = (function () {
                function BaseController($scope, $rootScope) {
                    this.$scope = $scope;
                    this.$rootScope = $rootScope;
                    // let scopeName = "scope";
                    var constructor = this.constructor;
                    var scopeName = constructor.name;
                    if (constructor.name.match(/controller/i)) {
                        scopeName = constructor.name.replace(/controller/i, '').toLowerCase();
                    }
                    //let scopeName = $rootScope.page;
                    this.$scope[scopeName] = this;
                }
                BaseController.$inject = ["$scope", "$rootScope"];
                return BaseController;
            }());
            exports_1("default",BaseController);
        }
    }
});
//# sourceMappingURL=BaseController.js.map