/**
 * This is the base controller class.
 *
 * Auto-injects the angular $scope and creates a reference to it,
 * using the state name by convention
 *
 */
abstract class BaseController {
    static $inject = ["$scope", "$rootScope"];


    constructor(protected $scope, protected $rootScope) {
        // let scopeName = "scope";
        let constructor:any = this.constructor;
        let scopeName = constructor.name;

        if (constructor.name.match(/controller/i)) {
            scopeName = constructor.name.replace(/controller/i, '').toLowerCase();
        }

        //let scopeName = $rootScope.page;
        this.$scope[scopeName] = this;
    }
}
export default BaseController;