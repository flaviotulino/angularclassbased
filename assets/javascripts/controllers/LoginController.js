System.register(["./BaseController"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __extends = (this && this.__extends) || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
    var BaseController_1;
    var LoginController;
    return {
        setters:[
            function (BaseController_1_1) {
                BaseController_1 = BaseController_1_1;
            }],
        execute: function() {
            LoginController = (function (_super) {
                __extends(LoginController, _super);
                function LoginController($scope, $rootScope, $state, $http) {
                    _super.call(this, $scope, $rootScope);
                    this.$scope = $scope;
                    this.$rootScope = $rootScope;
                    this.$state = $state;
                    this.$http = $http;
                    this.model = {
                        email: "test@test.com",
                        password: "11",
                        rememberMe: false
                    };
                    // new api.User(this.$http).test().then((r)=> {
                    //     console.log(r);
                    // })
                }
                LoginController.prototype.submit = function () {
                    console.log('submit');
                    this.$state.go('dashboard.home');
                };
                LoginController.$inject = ['$scope', '$rootScope', "$state", '$http'];
                return LoginController;
            }(BaseController_1.default));
            exports_1("default",LoginController);
        }
    }
});
//# sourceMappingURL=LoginController.js.map