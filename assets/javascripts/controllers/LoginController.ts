import BaseController from "./BaseController";
import {User} from "../../../api/User";
import {IRootScope} from "../customRun";
interface LoginModel {
    email:string;
    password:string;
    rememberMe:boolean;
}

class LoginController extends BaseController {

    private model:LoginModel;

    static $inject = ['$scope', '$rootScope', "$state", '$http'];

    constructor(protected $scope, protected $rootScope:IRootScope, protected $state, protected $http) {
        super($scope, $rootScope);
        this.model = {
            email: "test@test.com",
            password: "11",
            rememberMe: false
        };


        // new api.User(this.$http).test().then((r)=> {
        //     console.log(r);
        // })
    }


    public submit() {
        console.log('submit');
        this.$state.go('dashboard.home');
    }
}

export default LoginController;


