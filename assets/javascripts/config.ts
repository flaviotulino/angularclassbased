import Routes from "./routes";
function config($stateProvider) {
    Routes.routes.map((route:any) => {
        if (route.abstract) {
            if (route.templateUrl == undefined && route.template == undefined) {
                route.template = "<div ui-view></div>"
            }
        } else {
            route.templateUrl = route.templateUrl || "/views/" + route.name + ".html";
        }

        console.log(route);

        $stateProvider.state(route);
    })
}
config.$inject = ["$stateProvider"];
export default config;
