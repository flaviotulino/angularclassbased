System.register(["angular", "angular-ui-router", "./config", "./run", "./customRun", "./directives/directives", "./services/services"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var config_1, run_1, customRun_1;
    var application;
    return {
        setters:[
            function (_1) {},
            function (_2) {},
            function (config_1_1) {
                config_1 = config_1_1;
            },
            function (run_1_1) {
                run_1 = run_1_1;
            },
            function (customRun_1_1) {
                customRun_1 = customRun_1_1;
            },
            function (_3) {},
            function (_4) {}],
        execute: function() {
            application = angular
                .module('app', ['ui.router']);
            Object.keys(directives).map(function (name) {
                application.directive(name, directives[name]);
            });
            Object.keys(services).map(function (name) {
                application.service(name, services[name]);
            });
            application
                .config(config_1.default)
                .run(run_1.default)
                .run(customRun_1.default);
            angular.element(document).ready(function () {
                angular.bootstrap(document, ['app']);
            });
        }
    }
});
//# sourceMappingURL=application.js.map