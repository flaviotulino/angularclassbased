///<reference path="services/AuthService.ts"/>
function customRun($rootScope, AuthService) {
    $rootScope.AuthService = AuthService;
}
customRun.$inject = ['$rootScope', "AuthService"];
export default customRun;


export interface IRootScope {
    AuthService:services.AuthService;
}