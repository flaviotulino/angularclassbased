/**
 * Use this class to provide interpolation when needed
 */
class I18n {
    static t(string, args) {
        if (angular.isDefined(args)) {
            let keys = Object.keys(args);

            // For each argument passed as object
            keys.map(key => {
                // replace %{SOME_KEY_HERE} with its value
                let expr = new RegExp("\%\{" + key + "\}");
                string = string.replace(expr, args[key]);
            });
        }

        return string;
    }
}
export default I18n;