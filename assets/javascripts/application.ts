///<reference path='../../typings/angularjs/angular.d.ts' />
///<reference path='../../typings/angular-ui-router/angular-ui-router.d.ts' />
import "angular";
import "angular-ui-router";
import config from "./config";
import run from "./run";
import customRun from "./customRun";

let application = angular
    .module('app', ['ui.router']);


declare var directives;
declare var services;
import "./directives/directives";
import "./services/services";



Object.keys(directives).map((name) => {
    application.directive(name, directives[name]);
});

Object.keys(services).map((name) => {
    application.service(name, services[name]);
});


application
    .config(config)
    .run(run)
    .run(customRun);


angular.element(document).ready(() => {
    angular.bootstrap(document, ['app']);
});



