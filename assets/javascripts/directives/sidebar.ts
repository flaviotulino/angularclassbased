/**
 * Replaces the <sidebar> tag with the relative view
 */

class SidebarController {
    static $inject = ['$scope'];

    constructor(private $scope) {
        $scope.openSidebar = () => {
            $scope.isOpen = true;
        }
    };
}

module directives {
    export function sidebar() {
        return {
            restrict: 'E',
            templateUrl: 'views/_sidebar.html',
            replace: true,
            controller: SidebarController
        }
    }
}

