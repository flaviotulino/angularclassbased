/**
 * This directive replaces the navbar custom tag with the relative view
 */
var directives;
(function (directives) {
    function navbar() {
        console.log('navbar directive');
        return {
            require: "sidebar",
            restrict: 'E',
            replace: true,
            templateUrl: 'views/_navbar.html'
        };
    }
    directives.navbar = navbar;
})(directives || (directives = {}));
//# sourceMappingURL=navbar.js.map