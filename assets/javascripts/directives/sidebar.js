/**
 * Replaces the <sidebar> tag with the relative view
 */
var SidebarController = (function () {
    function SidebarController($scope) {
        this.$scope = $scope;
        $scope.openSidebar = function () {
            $scope.isOpen = true;
        };
    }
    ;
    SidebarController.$inject = ['$scope'];
    return SidebarController;
}());
var directives;
(function (directives) {
    function sidebar() {
        return {
            restrict: 'E',
            templateUrl: 'views/_sidebar.html',
            replace: true,
            controller: SidebarController
        };
    }
    directives.sidebar = sidebar;
})(directives || (directives = {}));
//# sourceMappingURL=sidebar.js.map