/**
 * Defines the <page> tag which created a section using the state name as reference
 */
module directives {
    export function page() {
        return {
            restrict: "E",
            transclude: true,
            replace: true,
            template: "<section class='page {{page}}-view' ng-transclude></section>",
            link: ()=> {
            }
        }
    }
}

