/**
 * This directive replaces the navbar custom tag with the relative view
 */
var directives;
(function (directives) {
    function navbar() {
        console.log('navbar directive');
        return {
            require: "sidebar",
            restrict: 'E',
            replace: true,
            templateUrl: 'views/_navbar.html'
        };
    }
    directives.navbar = navbar;
})(directives || (directives = {}));
/**
 * Defines the <page> tag which created a section using the state name as reference
 */
var directives;
(function (directives) {
    function page() {
        return {
            restrict: "E",
            transclude: true,
            replace: true,
            template: "<section class='page {{page}}-view' ng-transclude></section>",
            link: function () {
            }
        };
    }
    directives.page = page;
})(directives || (directives = {}));
/**
 * Replaces the <sidebar> tag with the relative view
 */
var SidebarController = (function () {
    function SidebarController($scope) {
        this.$scope = $scope;
        $scope.openSidebar = function () {
            $scope.isOpen = true;
        };
    }
    ;
    SidebarController.$inject = ['$scope'];
    return SidebarController;
}());
var directives;
(function (directives) {
    function sidebar() {
        return {
            restrict: 'E',
            templateUrl: 'views/_sidebar.html',
            replace: true,
            controller: SidebarController
        };
    }
    directives.sidebar = sidebar;
})(directives || (directives = {}));
