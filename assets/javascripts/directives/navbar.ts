/**
 * This directive replaces the navbar custom tag with the relative view
 */

module directives {
    export function navbar() {
        console.log('navbar directive');
        return {
            require: "sidebar",
            restrict: 'E',
            replace: true,
            templateUrl: 'views/_navbar.html'
        }
    }
}


