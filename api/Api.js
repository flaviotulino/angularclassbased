System.register(["./config"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var config_1;
    var Api;
    return {
        setters:[
            function (config_1_1) {
                config_1 = config_1_1;
            }],
        execute: function() {
            /**
             * This class provides methods to fetch and send server data
             */
            Api = (function () {
                function Api($http) {
                    this.$http = $http;
                    // Configure the endpoint basing on the running environment
                    this.BASE_URL = window.location.hostname == "localhost" ? config_1.default.DEV_ENDPOINT : config_1.default.PRODUCTION_ENDPOINT;
                }
                /**
                 * Makes an HTTP request using the get method providing
                 * url interpolation with the BASE_URL
                 * @param url String
                 * @returns {any}
                 */
                Api.prototype.get = function (url) {
                    return this.$http.get(this.BASE_URL + url);
                };
                /**
                 * Makes an HTTP request using the post method providing
                 * url interpolation with the BASE_URL
                 * @param url String
                 * @param data Object
                 * @returns {any}
                 */
                Api.prototype.post = function (url, data) {
                    return this.$http.post(this.BASE_URL + url, data);
                };
                /**
                 * Makes an HTTP request using the put method providing
                 * url interpolation with the BASE_URL
                 * @param url String
                 * @param data Object
                 * @returns {any}
                 */
                Api.prototype.put = function (url, data) {
                    return this.$http.put(this.BASE_URL + url, data);
                };
                /**
                 * Makes an HTTP request using the delete method providing
                 * url interpolation with the BASE_URL
                 * @param url String
                 * @returns {any}
                 */
                Api.prototype.delete = function (url) {
                    return this.$http.delete(this.BASE_URL + url);
                };
                Api.$inject = ['$http'];
                return Api;
            }());
            exports_1("Api", Api);
        }
    }
});
//# sourceMappingURL=Api.js.map