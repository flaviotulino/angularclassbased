System.register(["./Api"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __extends = (this && this.__extends) || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
    var Api_1;
    var User;
    return {
        setters:[
            function (Api_1_1) {
                Api_1 = Api_1_1;
            }],
        execute: function() {
            User = (function (_super) {
                __extends(User, _super);
                function User() {
                    _super.apply(this, arguments);
                }
                User.prototype.test = function () {
                    return this.get('someurl');
                };
                return User;
            }(Api_1.Api));
            exports_1("User", User);
        }
    }
});
//# sourceMappingURL=User.js.map