System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var config;
    return {
        setters:[],
        execute: function() {
            /**
             * DOC URL : http://docspxie.ddns.net/
             */
            config = {
                DEV_ENDPOINT: 'http://pxie.ddns.net/api/',
                PRODUCTION_ENDPOINT: ''
            };
            exports_1("default",config);
        }
    }
});
//# sourceMappingURL=config.js.map